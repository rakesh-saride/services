package amazon.scrapper;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Scrapper {

	public static void main(String[] args) throws IOException {
		Scrapper scrapper = new Scrapper();

		String productLink = scrapper.getProductLink("8901030684784");

		if (null != productLink) {
			scrapper.getDetails(productLink);
		}
	}

	private String getProductLink(String searchKeyword) throws IOException {
		Document doc = Jsoup.connect("https://www.amazon.in/s/field-keywords=" + searchKeyword).ignoreContentType(true).timeout(10000)
				.userAgent(
						"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36")
				.maxBodySize(0).validateTLSCertificates(false).get();

		Element content = doc.getElementById("atfResults");
		if (null != content) {
			Elements links = content.getElementsByTag("a");

			for (Element element : links) {
				if (element.attr("class").equals("a-link-normal a-text-normal")) {
					return element.attr("href");
				}
			}
		}

		return null;
	}

	private void getDetails(String productLink) throws IOException {
		Document doc = Jsoup.connect(productLink).ignoreContentType(true).timeout(20000).userAgent(
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36")
				.maxBodySize(0).validateTLSCertificates(false).get();

		Element content = doc.getElementById("wayfinding-breadcrumbs_feature_div");
		if (null != content) {
			Elements links = content.getElementsByTag("a");
			for (Element link : links) {
				String linkHref = link.attr("href");
				String linkText = link.text();

				System.out.println(linkText);
			}
		}

		Element title = doc.getElementById("productTitle");
		System.out.println(title.text());

		Element description = doc.getElementById("productDescription");
		System.out.println(description.text());
	}

}
