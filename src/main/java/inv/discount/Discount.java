package inv.discount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Discount {

	static class DiscountByTotal {
		public BigDecimal totalInvoiceAmount;

		public BigDecimal discount;

		public BigDecimal discountPct;

		@Override
		public String toString() {
			return "DiscountByTotal [totalInvoiceAmount=" + totalInvoiceAmount + ", discount=" + discount + ", discountPct=" + discountPct
					+ "]";
		}

	}

	static class DiscountByProductQty {
		public String productId;

		public int minimumQty;

		public BigDecimal discount;

		public BigDecimal discountPct;

		@Override
		public String toString() {
			return "DiscountByProductQty [productId=" + productId + ", minimumQty=" + minimumQty + ", discount=" + discount
					+ ", discountPct=" + discountPct + "]";
		}

	}

	static class DiscountByProductCombo {
		public List<String> products;

		public BigDecimal discount;

		public BigDecimal discountPct;

		@Override
		public String toString() {
			return "DiscountByProductCombo [products=" + products + ", discount=" + discount + ", discountPct=" + discountPct + "]";
		}

	}

	@Id
	private String id;

	@Indexed
	private String storeId;

	@NotBlank
	private String description;

	private DiscountByTotal discountByTotal;

	private DiscountByProductQty discountByProductQty;

	private DiscountByProductCombo discountByProductCombo;

	private Date startDate;

	private Date endDate;

	@CreatedBy
	private String createdBy;

	@CreatedDate
	private Date createdDate;

	@LastModifiedBy
	private String lastModifiedBy;

	@LastModifiedDate
	private Date lastModifiedDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DiscountByTotal getDiscountByTotal() {
		return discountByTotal;
	}

	public void setDiscountByTotal(DiscountByTotal discountByTotal) {
		this.discountByTotal = discountByTotal;
	}

	public DiscountByProductQty getDiscountByProductQty() {
		return discountByProductQty;
	}

	public void setDiscountByProductQty(DiscountByProductQty discountByProductQty) {
		this.discountByProductQty = discountByProductQty;
	}

	public DiscountByProductCombo getDiscountByProductCombo() {
		return discountByProductCombo;
	}

	public void setDiscountByProductCombo(DiscountByProductCombo discountByProductCombo) {
		this.discountByProductCombo = discountByProductCombo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@Override
	public String toString() {
		return "Discount [id=" + id + ", storeId=" + storeId + ", description=" + description + ", discountByTotal=" + discountByTotal
				+ ", discountByProductQty=" + discountByProductQty + ", discountByProductCombo=" + discountByProductCombo + ", startDate="
				+ startDate + ", endDate=" + endDate + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", lastModifiedBy="
				+ lastModifiedBy + ", lastModifiedDate=" + lastModifiedDate + "]";
	}

}
