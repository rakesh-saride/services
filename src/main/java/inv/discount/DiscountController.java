package inv.discount;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "discount")
public class DiscountController {

	@Autowired
	private DiscountRepository discountRepository;

	@PostMapping
	public String save(@Validated @RequestBody Discount discount, Principal principal) {
		String storeId = principal.getName().split(":")[1];
		discount.setStoreId(storeId);

		Discount discountInserted = discountRepository.insert(discount);

		return discountInserted.getId();
	}

	@PutMapping
	public void update(@Validated @RequestBody Discount discount) {
		discountRepository.save(discount);
	}

	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable String id) {
		discountRepository.deleteById(id);
	}

	@GetMapping(value = "/{id}")
	public Discount findById(@PathVariable("id") String id) {
		return discountRepository.findById(id).get();
	}

	@GetMapping(value = "/store")
	public List<Discount> findByStoreId(Principal principal) {
		String storeId = principal.getName().split(":")[1];
		return discountRepository.findByStoreId(storeId);
	}

}