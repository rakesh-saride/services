package inv.discount;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface DiscountRepository extends MongoRepository<Discount, String> {

	public List<Discount> findByStoreId(String storeId);
}