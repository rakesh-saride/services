package inv.discount;

public enum DiscountTypes {

	TOTAL(100), PRODUCT_QUANTITY(101), COMBO(102);

	private int typeCode;

	DiscountTypes(int typeCode) {
		this.typeCode = typeCode;
	}

	public int getTypeCode() {
		return typeCode;
	}

}
