package inv.eventlisteners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import inv.product.dto.Product;

@Component
public class ProductModelListener extends AbstractMongoEventListener<Product> {

	@Autowired
	private SequenceGenerator sequenceGenerator;

	@Override
	public void onBeforeConvert(BeforeConvertEvent<Product> event) {
		String storeId = event.getSource().getStoreId();
		event.getSource().setId("S" + storeId + "P" + sequenceGenerator.generateSequence(storeId + "_product_seq"));
	}

}
