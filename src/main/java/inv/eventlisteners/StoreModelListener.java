package inv.eventlisteners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import inv.inventory.Store;

@Component
public class StoreModelListener extends AbstractMongoEventListener<Store> {

	@Autowired
	private SequenceGenerator sequenceGenerator;

	@Override
	public void onBeforeConvert(BeforeConvertEvent<Store> event) {
		event.getSource().setId(sequenceGenerator.generateSequence(Store.SEQUENCE_NAME));
	}

}
