package inv.inventory;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "inventory")
public class InventoryController {

	@Autowired
	private InventoryRepository inventoryRepository;

	@Autowired
	private InventoryUpdateRepository inventoryUpdateRepository;

	@PostMapping
	public String save(@Validated @RequestBody Inventory inventory, Principal principal) {
		String storeId = principal.getName().split(":")[1];
		inventory.setStoreId(storeId);

		return inventoryUpdateRepository.save(inventory);
	}

	@PutMapping
	public void update(@Validated @RequestBody Inventory inventory) {
		inventoryRepository.save(inventory);
	}

	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable String id) {
		inventoryRepository.deleteById(id);
	}

	@GetMapping(value = "/{id}")
	public Inventory findById(@PathVariable("id") String id) {
		return inventoryRepository.findById(id).get();
	}

	@GetMapping(value = "/store")
	public List<Inventory> findByStoreId(Principal principal) {
		String storeId = principal.getName().split(":")[1];
		return inventoryRepository.findByStoreId(storeId);
	}

}