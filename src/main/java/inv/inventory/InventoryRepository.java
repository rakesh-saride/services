package inv.inventory;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface InventoryRepository extends MongoRepository<Inventory, String> {

	public List<Inventory> findByStoreId(String storeId);
}