package inv.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class InventoryUpdateRepository {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Transactional
	public String save(Inventory inventory) {
		if ("Y".equals(inventory.getIsPrimary())) {
			Query query = new Query();
			query.addCriteria(Criteria.where("storeId").is(inventory.getStoreId()));
			query.addCriteria(Criteria.where("isPrimary").is("Y"));

			Update update = new Update();
			update.set("isPrimary", "N");

			mongoTemplate.findAndModify(query, update, Inventory.class);
		}

		return mongoTemplate.save(inventory).getId();
	}
}
