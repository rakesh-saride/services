package inv.product;

import java.io.IOException;
import java.security.Principal;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import inv.product.dto.Product;
import inv.product.dto.ProductImage;
import inv.product.dto.ProductInventoryDetails;
import inv.product.dto.ProductUnit;
import inv.product.repository.ProductImageRepository;
import inv.product.repository.ProductRepository;
import inv.product.repository.ProductSearchRepository;
import inv.product.repository.ProductUpdateRepository;

@RestController
@RequestMapping(value = "product")
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductImageRepository productImageDAO;

	@Autowired
	private ProductSearchRepository productSearchRepository;

	@Autowired
	private ProductUpdateRepository productUpdateRepository;

	private List<String> units = EnumSet.allOf(ProductUnit.class).stream().map(unit -> unit.getDisplayName()).collect(Collectors.toList());

	@PostMapping("/image/upload")
	public String imageUpload(@RequestParam("file") MultipartFile multipart) throws IOException {
		ProductImage productImage = new ProductImage();
		productImage.setFile(new Binary(BsonBinarySubType.BINARY, multipart.getBytes()));
		ProductImage insertedProductImage = productImageDAO.insert(productImage);

		return insertedProductImage.getId();
	}

	@PostMapping
	public String save(@Validated @RequestBody Product product, Principal principal) {
		String storeId = principal.getName().split(":")[1];
		product.setStoreId(storeId);

		product.setSuffixes();

		Product productInserted = productRepository.insert(product);

		return productInserted.getId();
	}

	@PutMapping
	public void update(@Validated @RequestBody Product product) {
		productRepository.save(product);
	}

	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable String id) {
		productRepository.deleteById(id);
	}

	@GetMapping(value = "/{id}")
	public Product findById(@PathVariable("id") String id) {
		return productRepository.findById(id).get();
	}

	@GetMapping
	public List<Product> search(@RequestParam(value = "q", required = false) String query,
			@RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "pagesize", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sortDirection", required = false) String sortDirection,
			@RequestParam(value = "sortBy", required = false) String sortBy,
			@RequestParam(value = "inventoryInfoOnly", required = false) String inventoryInfoOnly, Principal principal) {

		String storeId = principal.getName().split(":")[1];

		if ("Y".equalsIgnoreCase(inventoryInfoOnly)) {
			return productSearchRepository.searchAndGetInventoryInfo(storeId, query, page, size, sortDirection, sortBy);
		}

		return productSearchRepository.search(storeId, query, page, size, sortDirection, sortBy);
	}

	@GetMapping(value = "/category")
	public List<Product> searchByCategory(@RequestParam(value = "q", required = false) String category, Principal principal) {

		String storeId = principal.getName().split(":")[1];

		return productSearchRepository.searchByCategory(storeId, category);
	}

	@GetMapping(value = "/categories")
	public List<String> getCategories(Principal principal) {
		String storeId = principal.getName().split(":")[1];
		return productSearchRepository.getCategories(storeId);
	}

	@PutMapping(value = "/inventory")
	public void updateInventory(@RequestBody List<ProductInventoryDetails> productUpdates) {
		productUpdateRepository.updateInventory(productUpdates);
	}

	@GetMapping(value = "/units")
	public List<String> getUnits() {
		return units;
	}

}