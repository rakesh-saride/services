package inv.product.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class InventoryInfo {

	@NotBlank
	private String inventoryId;

	@NotNull
	private Integer availableStock;

	private Integer reorderLevel;

	public String getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(String inventoryId) {
		this.inventoryId = inventoryId;
	}

	public Integer getAvailableStock() {
		return availableStock;
	}

	public void setAvailableStock(Integer availableStock) {
		this.availableStock = availableStock;
	}

	public Integer getReorderLevel() {
		return reorderLevel;
	}

	public void setReorderLevel(Integer reorderLevel) {
		this.reorderLevel = reorderLevel;
	}

	@Override
	public String toString() {
		return "InventoryInfo [inventoryId=" + inventoryId + ", availableStock=" + availableStock + ", reorderLevel=" + reorderLevel + "]";
	}

}
