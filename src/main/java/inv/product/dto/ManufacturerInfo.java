package inv.product.dto;

import javax.validation.constraints.NotBlank;

import org.springframework.data.mongodb.core.index.TextIndexed;

public class ManufacturerInfo {

	@NotBlank
	private String manufacturer;

	private String brand;

	@TextIndexed(weight = 4)
	private String upc;

	@TextIndexed(weight = 4)
	private String isbn;

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "ManufacturerInfo [manufacturer=" + manufacturer + ", brand=" + brand + ", upc=" + upc + ", isbn=" + isbn + "]";
	}

}
