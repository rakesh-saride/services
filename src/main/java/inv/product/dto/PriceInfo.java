package inv.product.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

public class PriceInfo {

	@NotNull
	private BigDecimal sellingPrice;

	@NotNull
	private BigDecimal costPrice;

	@NotNull
	private BigDecimal tax;

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public BigDecimal getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	@Override
	public String toString() {
		return "SalesInfo [sellingPrice=" + sellingPrice + ", costPrice=" + costPrice + ", tax=" + tax + "]";
	}

}
