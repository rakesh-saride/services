package inv.product.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

@Document
public class Product {

	@Indexed
	private String id;

	@Indexed
	private String storeId;

	@NotBlank
	private String name;

	@NotBlank
	private String category;

	private BigDecimal unitQuantity;

	private String unit;

	@TextIndexed(weight = 2)
	private Set<String> tags;

	private String description;

	private ManufacturerInfo manufacturerInfo;

	private PriceInfo priceInfo;

	private Map<String, InventoryInfo> inventoryInfo;

	private String imageId;

	@CreatedBy
	private String createdBy;

	@CreatedDate
	private Date createdDate;

	@LastModifiedBy
	private String lastModifiedBy;

	@LastModifiedDate
	private Date lastModifiedDate;

	@TextIndexed(weight = 4)
	private Set<String> nameSuffixes;

	@TextIndexed(weight = 2)
	private Set<String> manufacturerSuffixes;

	@TextIndexed(weight = 2)
	private Set<String> brandSuffixes;

	@TextScore
	private Float score;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public BigDecimal getUnitQuantity() {
		return unitQuantity;
	}

	public void setUnitQuantity(BigDecimal unitQuantity) {
		this.unitQuantity = unitQuantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ManufacturerInfo getManufacturerInfo() {
		return manufacturerInfo;
	}

	public void setManufacturerInfo(ManufacturerInfo manufacturerInfo) {
		this.manufacturerInfo = manufacturerInfo;
	}

	public PriceInfo getPriceInfo() {
		return priceInfo;
	}

	public void setPriceInfo(PriceInfo priceInfo) {
		this.priceInfo = priceInfo;
	}

	public Map<String, InventoryInfo> getInventoryInfo() {
		return inventoryInfo;
	}

	public void setInventoryInfo(Map<String, InventoryInfo> inventoryInfo) {
		this.inventoryInfo = inventoryInfo;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Set<String> getNameSuffixes() {
		return nameSuffixes;
	}

	public void setNameSuffixes(Set<String> nameSuffixes) {
		this.nameSuffixes = nameSuffixes;
	}

	public Set<String> getManufacturerSuffixes() {
		return manufacturerSuffixes;
	}

	public void setManufacturerSuffixes(Set<String> manufacturerSuffixes) {
		this.manufacturerSuffixes = manufacturerSuffixes;
	}

	public Set<String> getBrandSuffixes() {
		return brandSuffixes;
	}

	public void setBrandSuffixes(Set<String> brandSuffixes) {
		this.brandSuffixes = brandSuffixes;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public void setSuffixes() {
		this.setNameSuffixes(generateSuffixes(this.getName()));
		this.setManufacturerSuffixes(generateSuffixes(this.getManufacturerInfo().getManufacturer()));
		this.setBrandSuffixes(generateSuffixes(this.getManufacturerInfo().getBrand()));
	}

	private Set<String> generateSuffixes(String input) {
		if (null == input)
			return null;

		String[] inputArr = input.toLowerCase().split(" ");

		String temp;
		Set<String> suffixes = new HashSet<String>();
		for (String word : inputArr) {
			for (int i = 3; i <= word.length(); i++) {
				temp = word.substring(0, i);
				suffixes.add(temp);
			}
		}

		temp = null;

		return suffixes;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", storeId=" + storeId + ", name=" + name + ", category=" + category + ", unitQuantity=" + unitQuantity
				+ ", unit=" + unit + ", tags=" + tags + ", description=" + description + ", manufacturerInfo=" + manufacturerInfo
				+ ", priceInfo=" + priceInfo + ", inventoryInfo=" + inventoryInfo + ", imageId=" + imageId + ", createdBy=" + createdBy
				+ ", createdDate=" + createdDate + ", lastModifiedBy=" + lastModifiedBy + ", lastModifiedDate=" + lastModifiedDate
				+ ", nameSuffixes=" + nameSuffixes + ", manufacturerSuffixes=" + manufacturerSuffixes + ", brandSuffixes=" + brandSuffixes
				+ ", score=" + score + "]";
	}

}
