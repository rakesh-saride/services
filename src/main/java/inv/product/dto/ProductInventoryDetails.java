package inv.product.dto;

import java.util.Map;

public class ProductInventoryDetails {

	private String id;

	private Map<String, Integer> inventoryInfo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Integer> getInventoryInfo() {
		return inventoryInfo;
	}

	public void setInventoryInfo(Map<String, Integer> inventoryInfo) {
		this.inventoryInfo = inventoryInfo;
	}

	@Override
	public String toString() {
		return "ProductInventoryDetails [id=" + id + ", inventoryInfo=" + inventoryInfo + "]";
	}

}
