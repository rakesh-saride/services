package inv.product.dto;

public enum ProductUnit {
	KG("kg"), GM("gm"), LITRE("l"), ML("ml"), M("m"), CM("cm"), FEET("ft"), INCH("inch"), BOX("box"), PIECES("pieces");

	private String displayName;

	ProductUnit(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

}
