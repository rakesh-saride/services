package inv.product.repository;

import inv.product.dto.ProductImage;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductImageRepository extends MongoRepository<ProductImage, String> {
}
