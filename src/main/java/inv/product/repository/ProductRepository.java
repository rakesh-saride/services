package inv.product.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import inv.product.dto.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

}