package inv.product.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import inv.product.dto.Product;

@Repository
public class ProductSearchRepository {

	@Autowired
	private MongoTemplate mongoTemplate;

	public List<Product> search(String storeId, String input, int page, int size, String sortDirection, String sortBy) {
		if (StringUtils.isEmpty(input)) {
			Query query = new Query();
			query.addCriteria(Criteria.where("storeId").is(storeId));
			query.fields().exclude("nameSuffixes");
			query.fields().exclude("manufacturerSuffixes");
			query.fields().exclude("brandSuffixes");

			if (StringUtils.isEmpty(sortBy)) {
				query.with(new Sort(Sort.Direction.ASC, "name"));
			} else {
				query.with(new Sort(sortDirection == "desc" ? Sort.Direction.DESC : Sort.Direction.ASC, sortBy));
			}

			// TODO get count to return total result size.

			Pageable pageable = PageRequest.of(page, size);
			query.with(pageable);

			return mongoTemplate.find(query, Product.class);
		} else {
			TextCriteria criteria = TextCriteria.forDefaultLanguage().diacriticSensitive(false).matchingPhrase(input.toLowerCase());

			TextQuery query = new TextQuery(criteria);
			query.addCriteria(Criteria.where("storeId").is(storeId));
			query.setScoreFieldName("score");
			query.fields().exclude("nameSuffixes");
			query.fields().exclude("manufacturerSuffixes");
			query.fields().exclude("brandSuffixes");
			query.sortByScore();

			Pageable pageable = PageRequest.of(page, size);
			query.with(pageable);

			return mongoTemplate.find(query, Product.class);
		}
	}

	public List<Product> searchAndGetInventoryInfo(String storeId, String input, int page, int size, String sortDirection, String sortBy) {
		if (StringUtils.isEmpty(input)) {
			Query query = new Query();
			query.addCriteria(Criteria.where("storeId").is(storeId));
			query.fields().include("id");
			query.fields().include("name");
			query.fields().include("inventoryInfo");

			if (StringUtils.isEmpty(sortBy)) {
				query.with(new Sort(Sort.Direction.ASC, "name"));
			} else {
				query.with(new Sort(sortDirection == "desc" ? Sort.Direction.DESC : Sort.Direction.ASC, sortBy));
			}

			Pageable pageable = PageRequest.of(page, size);
			query.with(pageable);

			return mongoTemplate.find(query, Product.class);
		} else {
			TextCriteria criteria = TextCriteria.forDefaultLanguage().diacriticSensitive(false).matchingPhrase(input.toLowerCase());

			TextQuery query = new TextQuery(criteria);
			query.addCriteria(Criteria.where("storeId").is(storeId));
			query.setScoreFieldName("score");
			query.fields().include("id");
			query.fields().include("name");
			query.fields().include("inventoryInfo");
			query.sortByScore();

			Pageable pageable = PageRequest.of(page, size);
			query.with(pageable);

			return mongoTemplate.find(query, Product.class);
		}
	}

	public List<String> getCategories(String storeId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("storeId").is(storeId));

		List<String> categoryList = new ArrayList<>();
		MongoCollection<?> mongoCollection = mongoTemplate.getCollection("product");
		DistinctIterable<String> distinctIterable = mongoCollection.distinct("category", query.getQueryObject(), String.class);
		MongoCursor<String> cursor = distinctIterable.iterator();
		while (cursor.hasNext()) {
			String category = (String) cursor.next();
			categoryList.add(category);
		}

		return categoryList;
	}

	public List<Product> searchByCategory(String storeId, String category) {
		Query query = new Query();
		query.addCriteria(Criteria.where("storeId").is(storeId));
		query.addCriteria(Criteria.where("category").is(category));
		query.fields().exclude("nameSuffixes");
		query.fields().exclude("manufacturerSuffixes");
		query.fields().exclude("brandSuffixes");

		return mongoTemplate.find(query, Product.class);

	}
}
