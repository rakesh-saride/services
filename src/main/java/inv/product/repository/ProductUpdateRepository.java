package inv.product.repository;

import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import inv.product.dto.Product;
import inv.product.dto.ProductInventoryDetails;

@Repository
public class ProductUpdateRepository {

	@Autowired
	private MongoTemplate mongoTemplate;

	public void updateInventory(List<ProductInventoryDetails> productUpdates) {
		Query query;
		Update update;
		for (ProductInventoryDetails productUpdate : productUpdates) {
			query = new Query();
			query.addCriteria(Criteria.where("id").is(productUpdate.getId()));

			update = new Update();
			for (Entry<String, Integer> entry : productUpdate.getInventoryInfo().entrySet()) {
				update.inc("inventoryInfo." + entry.getKey() + ".availableStock", entry.getValue());
			}

			mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Product.class);
		}
	}

}
