package inv.sale;

import java.math.BigDecimal;

public class CustomDiscount {

	static class DiscountDetails {
		public BigDecimal discount;

		public BigDecimal discountPct;

		@Override
		public String toString() {
			return "DiscountDetails [discount=" + discount + ", discountPct=" + discountPct + "]";
		}

	}

	private String reason;

	private DiscountDetails discountForSelectedProduct;

	private DiscountDetails discountByTotal;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public DiscountDetails getDiscountForSelectedProduct() {
		return discountForSelectedProduct;
	}

	public void setDiscountForSelectedProduct(DiscountDetails discountForSelectedProduct) {
		this.discountForSelectedProduct = discountForSelectedProduct;
	}

	public DiscountDetails getDiscountByTotal() {
		return discountByTotal;
	}

	public void setDiscountByTotal(DiscountDetails discountByTotal) {
		this.discountByTotal = discountByTotal;
	}

	@Override
	public String toString() {
		return "CustomDiscount [reason=" + reason + ", discountForSelectedProduct=" + discountForSelectedProduct + ", discountByTotal="
				+ discountByTotal + "]";
	}

}
