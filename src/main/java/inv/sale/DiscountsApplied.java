package inv.sale;

import java.util.List;

public class DiscountsApplied {

	private List<CustomDiscount> customDiscounts;

	private List<String> discountByProduct;

	private List<String> discountByProductCombo;

	private List<String> discountByProductQty;

	private String discountByTotal;

	public List<CustomDiscount> getCustomDiscounts() {
		return customDiscounts;
	}

	public void setCustomDiscounts(List<CustomDiscount> customDiscounts) {
		this.customDiscounts = customDiscounts;
	}

	public List<String> getDiscountByProduct() {
		return discountByProduct;
	}

	public void setDiscountByProduct(List<String> discountByProduct) {
		this.discountByProduct = discountByProduct;
	}

	public List<String> getDiscountByProductCombo() {
		return discountByProductCombo;
	}

	public void setDiscountByProductCombo(List<String> discountByProductCombo) {
		this.discountByProductCombo = discountByProductCombo;
	}

	public List<String> getDiscountByProductQty() {
		return discountByProductQty;
	}

	public void setDiscountByProductQty(List<String> discountByProductQty) {
		this.discountByProductQty = discountByProductQty;
	}

	public String getDiscountByTotal() {
		return discountByTotal;
	}

	public void setDiscountByTotal(String discountByTotal) {
		this.discountByTotal = discountByTotal;
	}

	@Override
	public String toString() {
		return "DiscountsApplied [customDiscounts=" + customDiscounts + ", discountByProduct=" + discountByProduct
				+ ", discountByProductCombo=" + discountByProductCombo + ", discountByProductQty=" + discountByProductQty
				+ ", discountByTotal=" + discountByTotal + "]";
	}

}
