package inv.sale;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import inv.product.dto.ManufacturerInfo;
import inv.product.dto.PriceInfo;

public class ProductSaleDetails {

	@NotBlank
	private String id;

	@NotBlank
	private String name;

	@NotNull
	private int quantity;

	@NotBlank
	private String description;

	@NotNull
	private ManufacturerInfo manufacturerInfo;

	@NotNull
	private PriceInfo priceInfo;

	private BigDecimal discountAmount;

	// Array of discountId and custom discount objects.
	private Object discountsApplied;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ManufacturerInfo getManufacturerInfo() {
		return manufacturerInfo;
	}

	public void setManufacturerInfo(ManufacturerInfo manufacturerInfo) {
		this.manufacturerInfo = manufacturerInfo;
	}

	public PriceInfo getPriceInfo() {
		return priceInfo;
	}

	public void setPriceInfo(PriceInfo priceInfo) {
		this.priceInfo = priceInfo;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Object getDiscountsApplied() {
		return discountsApplied;
	}

	public void setDiscountsApplied(Object discountsApplied) {
		this.discountsApplied = discountsApplied;
	}

	@Override
	public String toString() {
		return "ProductSaleDetails [id=" + id + ", name=" + name + ", quantity=" + quantity + ", description=" + description
				+ ", manufacturerInfo=" + manufacturerInfo + ", priceInfo=" + priceInfo + ", discountAmount=" + discountAmount
				+ ", discountsApplied=" + discountsApplied + "]";
	}

}
