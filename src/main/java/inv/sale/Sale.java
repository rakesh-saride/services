package inv.sale;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Sale {

	static class PaymentDetails {
		public BigDecimal cash;

		public BigDecimal card;

		@Override
		public String toString() {
			return "PaymentDetails [cash=" + cash + ", card=" + card + "]";
		}
	}

	@Id
	private String id;

	@Indexed
	private String storeId;

	@NotBlank
	private String inventoryId;

	@NotNull
	private List<ProductSaleDetails> products;

	private DiscountsApplied discountsApplied;

	private BigDecimal totalBeforeTaxAndDiscount;

	private BigDecimal discount;

	private BigDecimal tax;

	private BigDecimal total;

	private PaymentDetails paymentDetails;

	@CreatedBy
	private String createdBy;

	@CreatedDate
	private Date createdDate;

	@LastModifiedBy
	private String lastModifiedBy;

	@LastModifiedDate
	private Date lastModifiedDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(String inventoryId) {
		this.inventoryId = inventoryId;
	}

	public List<ProductSaleDetails> getProducts() {
		return products;
	}

	public void setProducts(List<ProductSaleDetails> products) {
		this.products = products;
	}

	public DiscountsApplied getDiscountsApplied() {
		return discountsApplied;
	}

	public void setDiscountsApplied(DiscountsApplied discountsApplied) {
		this.discountsApplied = discountsApplied;
	}

	public BigDecimal getTotalBeforeTaxAndDiscount() {
		return totalBeforeTaxAndDiscount;
	}

	public void setTotalBeforeTaxAndDiscount(BigDecimal totalBeforeTaxAndDiscount) {
		this.totalBeforeTaxAndDiscount = totalBeforeTaxAndDiscount;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@Override
	public String toString() {
		return "Sale [id=" + id + ", storeId=" + storeId + ", inventoryId=" + inventoryId + ", products=" + products + ", discountsApplied="
				+ discountsApplied + ", totalBeforeTaxAndDiscount=" + totalBeforeTaxAndDiscount + ", discount=" + discount + ", tax=" + tax
				+ ", total=" + total + ", paymentDetails=" + paymentDetails + ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", lastModifiedBy=" + lastModifiedBy + ", lastModifiedDate=" + lastModifiedDate + "]";
	}

}
