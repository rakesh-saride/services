package inv.sale;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inv.product.dto.ProductInventoryDetails;
import inv.product.repository.ProductUpdateRepository;

@RestController
@RequestMapping(value = "sale")
public class SaleController {

	@Autowired
	private SaleRepository saleRepository;

	@Autowired
	private ProductUpdateRepository productUpdateRepository;

	@PostMapping
	public String save(@Validated @RequestBody Sale sale, Principal principal) {
		String storeId = principal.getName().split(":")[1];
		sale.setStoreId(storeId);

		Sale saleInserted = saleRepository.insert(sale);

		List<ProductInventoryDetails> inventoryUpdates = new ArrayList<>();
		ProductInventoryDetails productInventoryDetails;
		Map<String, Integer> inventoryInfo;

		for (ProductSaleDetails product : sale.getProducts()) {
			productInventoryDetails = new ProductInventoryDetails();
			productInventoryDetails.setId(product.getId());
			inventoryInfo = new HashMap<>();
			inventoryInfo.put(sale.getInventoryId(), -product.getQuantity());
			productInventoryDetails.setInventoryInfo(inventoryInfo);

			inventoryUpdates.add(productInventoryDetails);
		}

		productUpdateRepository.updateInventory(inventoryUpdates);

		return saleInserted.getId();
	}

	@PutMapping
	public void update(@Validated @RequestBody Sale sale) {
		saleRepository.save(sale);
	}

	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable String id) {
		saleRepository.deleteById(id);
	}

	@GetMapping(value = "/{id}")
	public Sale findById(@PathVariable("id") String id) {
		return saleRepository.findById(id).get();
	}

	@GetMapping(value = "/store")
	public List<Sale> findByStoreId(Principal principal) {
		String storeId = principal.getName().split(":")[1];
		return saleRepository.findByStoreId(storeId);
	}

}