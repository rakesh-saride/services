package inv.sale;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface SaleRepository extends MongoRepository<Sale, String> {

	public List<Sale> findByStoreId(String storeId);
}