package inv.user;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inv.inventory.Inventory;
import inv.inventory.InventoryRepository;
import inv.inventory.Store;
import inv.inventory.StoreRepository;
import inv.security.ApplicationUser;
import inv.security.ApplicationUserRepository;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private ApplicationUserRepository applicationUserRepository;

	@Autowired
	private StoreRepository storeRepository;

	@Autowired
	private InventoryRepository inventoryRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@PostMapping("/signup")
	public String signUp(@RequestBody User user) {
		ApplicationUser applicationUser = new ApplicationUser();
		applicationUser.setUsername(user.getUsername());
		applicationUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

		applicationUserRepository.save(applicationUser);

		Store store = storeRepository.save(user.getStoreInfo());
		user.setStoreId(store.getId());

		Inventory inventory = new Inventory();
		inventory.setName(store.getInventoryName());
		inventory.setStoreId(store.getId());
		inventory.setIsPrimary("Y");

		inventoryRepository.save(inventory);

		userRepository.save(user);

		return user.getUsername();
	}

	@GetMapping("/details")
	public User getLoggedinUserDetails(Principal principal) {
		String username = principal.getName().split(":")[0];

		User user = userRepository.findByUsername(username);

		if (null != user) {
			user.setStoreInfo(storeRepository.findById(user.getStoreId()).get());
		}

		return user;
	}

}
